import * as React from "react";
import Button from "@mui/material/Button";
import Form from "../../component/Form";

const pages = () => {
  return (
    <div>
      <Form />
      <h1>Page</h1>
      <Button variant="text">Text</Button>
      <Button variant="contained">Contained</Button>
      <Button variant="outlined">Outlined</Button>
    </div>
  );
};

export default pages;
