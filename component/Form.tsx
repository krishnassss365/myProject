import TextField from '@mui/material/TextField';


import React from 'react';
const page = () => {
  return (
    <TextField
    disabled
    id="outlined-disabled"
    label="Disabled"
    defaultValue="Hello World"
  />
  );
};

export default page;